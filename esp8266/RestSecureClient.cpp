#include "RestSecureClient.h"
#include "certificates.h"


#define REST_SECURE_CLIENT_DEBUG

#ifdef REST_SECURE_CLIENT_DEBUG
   #define DEBUG_PRINTF(format, ...) (Serial.printf(format, __VA_ARGS__))
#else
   #define DEBUG_PRINTF(format, ...)
#endif

// Constructor
RestSecureClient::RestSecureClient(const char* _host) {
   init(_host, 80);
}

// Constructor
RestSecureClient::RestSecureClient(const char* _host, int _port) {
   init(_host, _port);
}

// init (private, called from constructors
void RestSecureClient::init(const char *_host, int _port) {
   host = _host;
   port = _port;
   num_headers = 0;
   contentType = "application/json";

#ifdef USE_SSL_CERTIFICATES
//   client.setCertificate(esp8266_bin_crt, esp8266_bin_crt_len);
//   client.setPrivateKey(esp8266_bin_key, esp8266_bin_key_len);
#endif
}

// GET path
int RestSecureClient::get(const char* path){
   return request("GET", path, NULL, NULL);
}

//GET path with response
int RestSecureClient::get(const char* path, char* response){
   return request("GET", path, NULL, response);
}

// POST path and body
int RestSecureClient::post(const char* path, const char* body){
   return request("POST", path, body, NULL);
}

// POST path and body with response
int RestSecureClient::post(const char* path, const char* body, char *response){
   return request("POST", path, body, response);
}

// PUT path and body
int RestSecureClient::put(const char* path, const char* body){
   return request("PUT", path, body, NULL);
}

// PUT path and body with response
int RestSecureClient::put(const char* path, const char* body, char* response){
   return request("PUT", path, body, response);
}

// DELETE path
int RestSecureClient::del(const char* path){
   return request("DELETE", path, NULL, NULL);
}

// DELETE path and response
int RestSecureClient::del(const char* path, char* response){
   return request("DELETE", path, NULL, response);
}

// DELETE path and body
int RestSecureClient::del(const char* path, const char* body ){
   return request("DELETE", path, body, NULL);
}

// DELETE path and body with response
int RestSecureClient::del(const char* path, const char* body, char* response){
   return request("DELETE", path, body, response);
}

// Set Header
void RestSecureClient::setHeader(const char* header){
   headers[num_headers] = header;
   num_headers++;
}

// Set contentType
void RestSecureClient::setContentType(const char* contentTypeValue){
   contentType = contentTypeValue;
}

//
int RestSecureClient::request(const char* method, const char* path,
                        const char* body, char *response){

   char req[512];
   
   DEBUG_PRINTF("%s:HTTPS: try to connect\n", __FUNCTION__);
   
   //
   if(!client.connect(host, port)){
      DEBUG_PRINTF("%s:HTTPS Connection failed\n", __FUNCTION__);
      return 0; 
   }
   
   DEBUG_PRINTF("%s:HTTPS: connected!\n", __FUNCTION__);
   client.connect(host, port);
   
   strcpy(req, method);
   strcat(req, " ");
   strcat(req, path);
   strcat(req, " HTTP/1.1\r\n");
   
   for(int idx = 0; idx < num_headers; idx++){
      strcat(req, headers[idx]);
      strcat(req, "\r\n");
   }
   
   strcat(req, "Host: "); strcat(req, host); strcat(req, "\r\n");
   strcat(req, "Connection: close\r\n");
   char buf[10];
   itoa(strlen(body), buf, 10);
   strcat(req, "Content-Length: "); strcat(req, buf); strcat(req, "\r\n");
   strcat(req, "Content-Type: "); strcat(req, contentType); strcat(req, "\r\n");
   strcat(req, "\r\n");
   
   strcat(req, body);
   strcat(req, "\r\n\r\n");
   
   client.print(req);
   
   // get response
   int statusCode = readResponse(response);
   
   // cleanup and stop
   num_headers = 0;
   
   client.stop();
   delay(50);
   
   return statusCode;
}

//
int RestSecureClient::readResponse(char *response) {

   boolean currentLineIsBlank = true;
   boolean httpBody = false;
   boolean inStatus = false;
   char *ptr_response = response; 
   
   char statusCode[4];
   int i = 0;
   int code = 0;
    
   while (client.connected()) {
      if (client.available()) {
         char ch = client.read();

         if(ch == ' ' && !inStatus){
            inStatus = true;
         }

         if(inStatus && i < 3 && ch != ' '){
            statusCode[i] = ch;
            i++;
         }
         if(i == 3){
            statusCode[i] = '\0';
            code = atoi(statusCode);
         }

         if(httpBody){
            if(response != NULL) {
               *ptr_response++ = ch; 
            }
         }
         else
         {
            if (ch == '\n' && currentLineIsBlank) {
               httpBody = true;
            }
         
            if (ch == '\n') {
               // you're starting a new line
               currentLineIsBlank = true;
            }
            else if (ch != '\r') {
               // you've gotten a character on the current line
               currentLineIsBlank = false;
            }
         }
      }
   }
  
   // Terminate response array
   *ptr_response = '\0';

   // Return code
   return code;
}

