 
//
// Smart Meter P1 to WIFI interface
// dkroeske@gmail.com
// Disclamer : 
#include <NeoPixelBus.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include "WiFiManager.h"
#include <Ticker.h>
#include <ESP8266mDNS.h>
#include <ArduinoJson.h>
#include <FS.h>
//#include "RestClient.h"
#include "RestSecureClient.h"


//
// situation | Mode
// ----------|-----
// Normal    | GPIO2 is used to control the NeoPixel
// Debug     | GPIO2 is connected to Serial1 to print debug messages
#define DEBUG

#ifdef DEBUG
 #define DEBUG_PRINTF(format, ...) (Serial1.printf(format, __VA_ARGS__))
#else
 #define DEBUG_PRINTF
#endif

// WiFi RESET pin
#define RST_PIN         4
#define BLINK_0         14    // WeMos D5
#define BLINK_1         12    // WeMos D6

#define DATAGRAM_UPDATE_RATE_MS  30000

typedef struct {
   unsigned short in_0_10_sec;
   unsigned short in_1_10_sec;   
} PULSE_STRUCT;

PULSE_STRUCT pulse;

Ticker ticker;
uint32_t cur=0, prev=0;


// Neo pixel
#define NeoPixelCount   1     // Number of Neo pixels
#define NeoPixelPort    2     // Neo pixel GPIO2
#define colorSaturation 20    // Neo pixel brightness
#define colorFlashLevel 120   // Neo pixel brightness when flashing

#ifndef DEBUG
NeoPixelBus<NeoGrbFeature, NeoEsp8266Uart800KbpsMethod> neoPixel(NeoPixelCount, NeoPixelPort);
#endif

// Neo pixel color presets
RgbColor red(colorSaturation, 0, 0);
RgbColor green(0, colorSaturation, 0);
RgbColor blue(0, 0, colorSaturation);
RgbColor white(colorSaturation);
RgbColor black(0);

// Local variables

WiFiManager wifiManager;

typedef uint16_t HTTP_CODE;

//RestClient client = NULL;
RestSecureClient client = NULL;

//const char *sha1_fingerprint = "9E B7 4B 7A 6F 8C 65 35 FC 66 30 DE 86 15 CD 4F 8B 66 04 52";

// Application configs struct. 
bool shouldSaveConfig;

#define USERNAME_LENGTH       32
#define PASSWORD_LENGTH       32
#define SESSION_TOKEN_LENGTH 128
#define SIGNATURE_LENGTH      64
#define REMOTE_REMOTE_HOST   128
#define REMOTE_REMOTE_PORT    10
#define S_IMP_KWH             10

typedef struct {
   char     username[USERNAME_LENGTH];
   char     password[PASSWORD_LENGTH];
   char     logger_signature[SIGNATURE_LENGTH];
   char     api_session_token[SESSION_TOKEN_LENGTH];
   uint32_t api_client_id;
   uint32_t api_logger_id;
   char     remote_host[REMOTE_REMOTE_HOST];
   char     remote_port[REMOTE_REMOTE_PORT];
   char     s0_impkwh[S_IMP_KWH];
   char     s1_impkwh[S_IMP_KWH];
} APP_CONFIG_STRUCT;

APP_CONFIG_STRUCT app_config;

#define P1_TELEGRAM_SIZE   1024

// Datagram P1 buffer 
#define P1_MAX_DATAGRAM_SIZE 1024
char p1_buf[P1_MAX_DATAGRAM_SIZE]; // Complete P1 telegram
char *p1;

// P1 statemachine
typedef enum { 
   P1_MSG_S0,
   P1_MSG_S1,
   P1_MSG_S2
} ENUM_P1_MSG_STATE;
ENUM_P1_MSG_STATE p1_msg_state = P1_MSG_S0;

// 
typedef struct {
   char p1_telegram[P1_TELEGRAM_SIZE];
   uint16_t  s0_value;
   uint16_t  s1_value;
} MEASUREMENT_STRUCT;
MEASUREMENT_STRUCT payload = {"",0,0};

// Prototype state functions. 
void s0(void);
void s1(void);
void s2(void);
void s3(void);
void s4(void);
void err(void);

// Define FSM (states, events)
typedef enum { EV_TRUE = 0, EV_ERR, EV_TELE, EV_ILOG } ENUM_EVENT;
typedef enum { STATE_S0 = 0, STATE_S1, STATE_S2, STATE_S3, STATE_S4, STATE_ERR } ENUM_STATE;

/* Define fsm transition */
typedef struct {
   void (*f)(void);
   ENUM_STATE nextState;
} STATE_TRANSITION_STRUCT;

typedef enum {NO_ERR = 0, ERR_NETWORK, ERR_INVALID_CREDENTIALS } API_ERR_ENUM;
API_ERR_ENUM api_err = NO_ERR; 

// FSM definition (see statemachine diagram)
//
//       | EV_TRUE   EV_ERR   EV_TELE  EV_ILOG   
// -------------------------------------------------------------
// S0    | S1        ERROR    ERROR    ERROR
// S1    | S3        ERROR    ERROR    S2
// S2    | S3        ERROR    ERROR    ERROR
// S3    | ERROR     ERROR    S4       ERROR
// S4    | S3        ERROR    ERROR    ERROR
// ERROR | S0        ERROR    ERROR    ERROR

STATE_TRANSITION_STRUCT fsm[6][4] = {
   { {s1,   STATE_S1},  {err, STATE_ERR}, {err, STATE_ERR}, {err, STATE_ERR} }, // State S0
   { {s3,   STATE_S3},  {err, STATE_ERR}, {err, STATE_ERR}, {s2,   STATE_S2} }, // State S1
   { {s3,   STATE_S3},  {err, STATE_ERR}, {err, STATE_ERR}, {err, STATE_ERR} }, // State S2
   { {s3,   STATE_S3},  {err, STATE_ERR}, {s4,  STATE_S4},  {err, STATE_ERR} }, // State S3
   { {s3,   STATE_S3},  {err, STATE_ERR}, {err, STATE_ERR}, {err, STATE_ERR} }, // State S4
   { {s0,   STATE_S0},  {err, STATE_ERR}, {err, STATE_ERR}, {err,  STATE_ERR} } // State ERROR
};

// State holder
ENUM_STATE state = STATE_ERR;
ENUM_EVENT event = EV_TRUE;


/******************************************************************/
void saveConfigCallback () 
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   shouldSaveConfig = true;
}


/******************************************************************/
void setup() 
/* 
short:         initial setup(), runes only one time
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{    
   // Define I/O and attach ISR
   pinMode(RST_PIN, INPUT);

   // Set unique logger signature
   create_signature(app_config.logger_signature);

   // Init with red led
   smartLedInit(red);
   for(int idx = 0; idx < 5; idx++ ) {
      smartLedFlash(colorFlashLevel);
      delay(100);
   }

   // Perform factory reset switches
   // is pressed during powerup
   if( 0 == digitalRead(RST_PIN) ) {
      wifiManager.resetSettings();
      deleteAppConfig();
      while(0 == digitalRead(RST_PIN)) {
         smartLedFlash(colorFlashLevel);
         delay(500);
      }
      ESP.reset();
   }

   // Read config file or generate default
   if( !readAppConfig(&app_config) ) {
      strcpy(app_config.username, "");
      strcpy(app_config.password, "");
      strcpy(app_config.remote_host, "vps440555.ovh.net");
      strcpy(app_config.remote_port, "8080");
      strcpy(app_config.s0_impkwh, "1000");
      strcpy(app_config.s1_impkwh, "1000");
      writeAppConfig(&app_config);
   }

   //
   smartLedShowColor(blue);
   wifiManager.setMinimumSignalQuality(20);
   wifiManager.setTimeout(300);
   wifiManager.setSaveConfigCallback(saveConfigCallback);
   shouldSaveConfig = false;

   WiFiManagerParameter logger_text("<br/>Logger informatie:");
   wifiManager.addParameter(&logger_text);
   WiFiManagerParameter custom_username("username", "Username", app_config.username, USERNAME_LENGTH);
   WiFiManagerParameter custom_password("password", "Password", app_config.password, PASSWORD_LENGTH);
   WiFiManagerParameter custom_remote_host("remote_host", "Remote Host", app_config.remote_host, REMOTE_REMOTE_HOST);
   WiFiManagerParameter custom_remote_port("port", "Remote Port", app_config.remote_port, REMOTE_REMOTE_PORT);
   wifiManager.addParameter(&custom_username);
   wifiManager.addParameter(&custom_password);
   wifiManager.addParameter(&custom_remote_host);
   wifiManager.addParameter(&custom_remote_port);

   WiFiManagerParameter pulse_text("<br/>imp/kWh (e.g. 1000):");
   wifiManager.addParameter(&pulse_text);
   WiFiManagerParameter custom_s0("s0", "1000", app_config.s0_impkwh, S_IMP_KWH);
   WiFiManagerParameter custom_s1("s1", "1000", app_config.s1_impkwh, S_IMP_KWH);
   wifiManager.addParameter(&custom_s0);
   wifiManager.addParameter(&custom_s1);
   
   if( !wifiManager.autoConnect("Emon configuratie")) {
      delay(1000);
      ESP.reset();
   }

   //
   // Update config
   //
   if(shouldSaveConfig) {
      strcpy(app_config.username, custom_username.getValue());
      strcpy(app_config.password, custom_password.getValue());
      strcpy(app_config.remote_host, custom_remote_host.getValue());
      strcpy(app_config.remote_port, custom_remote_port.getValue());
      strcpy(app_config.s0_impkwh, custom_s0.getValue());
      strcpy(app_config.s1_impkwh, custom_s1.getValue());
      writeAppConfig(&app_config);
   }
   
   //
   if( !MDNS.begin("DIY-EMON_V10") ) {
   } else {
      MDNS.addService("diy_emon_v10", "tcp", 10000);
   }

   //
   noInterrupts();

   // Blink input 0
   pinMode(BLINK_0, INPUT);
   attachInterrupt(BLINK_0, blink_0_isr, RISING);

   // Blink input 1
   pinMode(BLINK_1, INPUT);
   attachInterrupt(BLINK_1, blink_1_isr, RISING);

   interrupts();

   // Handle statemachine trigger
   ticker.attach(10.0, tickerHandler);

   //
   //Serial.begin(9600, SERIAL_7E1);
   Serial.begin(115200, SERIAL_8N1);

   #ifdef DEBUG
   Serial1.begin(115200, SERIAL_8N1);
   Serial1.printf("\n\r... in debug mode ...\n\r");
   #endif

   delay(1000);

   // Relocate Serial Port
   Serial.swap();

   // Debug
   app_config.api_client_id = 0;
   app_config.api_logger_id = 0;

   // Debug
   DEBUG_PRINTF("SDK Version: %s\n\r", ESP.getSdkVersion() );
   DEBUG_PRINTF("CORE Version: %s\n\r", ESP.getCoreVersion().c_str() );
   DEBUG_PRINTF("RESET: %s\n\r", ESP.getResetReason().c_str() );
   DEBUG_PRINTF("%s:username:\t%s\n\r", __FUNCTION__, app_config.username);
   DEBUG_PRINTF("%s:password:\t%s\n\r", __FUNCTION__, app_config.password);
   DEBUG_PRINTF("%s:logger_signature:\t%s\n\r", __FUNCTION__, app_config.logger_signature);
   DEBUG_PRINTF("%s:local IP:\t%s\n\r", __FUNCTION__, WiFi.localIP().toString().c_str() );
   DEBUG_PRINTF("%s:remote_host:\t%s\n\r", __FUNCTION__, app_config.remote_host);
   DEBUG_PRINTF("%s:port_server:\t%d\n\r", __FUNCTION__, atol(app_config.remote_port));
   DEBUG_PRINTF("%s:S0 %d imp/kWh\n\r", __FUNCTION__, atol(app_config.s0_impkwh));
   DEBUG_PRINTF("%s:S1 %d imp/kWh\n\r", __FUNCTION__, atol(app_config.s1_impkwh));
   smartLedShowColor(green);

   delay(1000);

//   client = RestClient(app_config.remote_host, atoi(app_config.remote_port));
   client = RestSecureClient(app_config.remote_host, atoi(app_config.remote_port));
   client.setContentType("application/json");
   delay(1000);

   DEBUG_PRINTF("%s:freq: %d Mhz\n\r", __FUNCTION__, ESP.getCpuFreqMHz());
   delay(1000);
}

/******************************************************************/
/*
 * Application signature and config
 */
/******************************************************************/

/******************************************************************/
void create_signature(char *signature)
/* 
short:      Construct unique logger_signature    
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
   char tmp[30];
   strcpy(signature,"EMON");
   strcat(signature,"-V02");
   sprintf(tmp,"-%06X",ESP.getChipId());
   strcat(signature,tmp);
   sprintf(tmp,"-%06X",ESP.getFlashChipId()); 
   strcat(signature,tmp);
}

/******************************************************************/
bool readAppConfig(APP_CONFIG_STRUCT *app_config) 
/* 
short:         loop(), runs forever executing FSM
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   bool retval = false;
   
   if( SPIFFS.begin() ) {
      if( SPIFFS.exists("/config.json") ) {
         File file = SPIFFS.open("/config.json","r");
         if( file ) {
            size_t size = file.size();
            std::unique_ptr<char[]> buf(new char[size]);

            file.readBytes(buf.get(), size);
            DynamicJsonBuffer jsonBuffer;
            JsonObject& json = jsonBuffer.parseObject(buf.get());
            if( json.success() ) {
               strcpy(app_config->username, json["USERNAME"]);
               strcpy(app_config->password, json["LOGGER_NAME"]);
               strcpy(app_config->remote_host, json["REMOTE_HOST"]);
               strcpy(app_config->remote_port, json["REMOTE_PORT"]);
               strcpy(app_config->s0_impkwh, json["S0_IMP"]);
               strcpy(app_config->s1_impkwh, json["S1_IMP"]);

               retval = true;
            }
         }
      }
   } 
 
   return retval;
}

/******************************************************************/
bool writeAppConfig(APP_CONFIG_STRUCT *app_config) 
/* 
short:         Write config to FFS
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   bool retval = false;
   
   if( SPIFFS.begin() ) {
      
      // Delete config if exists
      if( SPIFFS.exists("/config.json") ) {
         SPIFFS.remove("/config.json");
      }

      // Create new and store settings
      DynamicJsonBuffer jsonBuffer;
      JsonObject& json = jsonBuffer.createObject();
      json["USERNAME"] = app_config->username;
      json["LOGGER_NAME"] = app_config->password;
      json["REMOTE_HOST"] = app_config->remote_host;
      json["REMOTE_PORT"] = app_config->remote_port;
      json["S0_IMP"] = app_config->s0_impkwh;
      json["S1_IMP"] = app_config->s1_impkwh;

      File file = SPIFFS.open("/config.json","w");
      if( file ) {
         json.printTo(file);
         file.close();
         retval = true;
      }
   } 
   return retval;
}

/******************************************************************/
void deleteAppConfig() 
/* 
short:         Erase config to FFS
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   if( SPIFFS.begin() ) {
      
      // Delete config if exists
      if( SPIFFS.exists("/config.json") ) {
         SPIFFS.remove("/config.json");
      }
   } 
}



/******************************************************************/
void loop()
/* 
short:         loop(), runs forever executing FSM
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  handleEvent();
}

/******************************************************************/
/*
 * Http section
 */
/******************************************************************/

/******************************************************************/
void url_encode(const char *in, char *out)
/* 
short:      Decode str to URL save string    
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{   
   char ch[5];

   // Make sure out is 'empty'
   out[0] = '\0';

   // Loop
   while(*in) {
      if( (*in >= '0' && *in <= '9') || 
         ( *in >= 'a' && *in <= 'z') || 
         ( *in >= 'A' && *in <= 'Z') ) {
         sprintf(ch, "%c", *in);
      } else {
         sprintf(ch, "%%%X", *in);
      }
      strcat(out, ch);
      in++;
   }
}

//String res="";
char res[1024];

/*******************************************************************/
HTTP_CODE api_v1_loggersForClientByName(APP_CONFIG_STRUCT *app_config)

/* 
short:         
inputs:        
outputs:       
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   //res = "";

   char url[512];
   char *buf = url;
   char urlencoded_filter[128];

   // Construct filter and url_encode (misuse url char[])
   sprintf(buf, "{\"where\":{\"signature\":\"%s\"}}",app_config->logger_signature);
   url_encode(buf, urlencoded_filter);

   // Construct url
   sprintf(url,"/api/v1/clients/%d/loggers?filter=%s&access_token=%s", 
      app_config->api_client_id, 
      urlencoded_filter,
      app_config->api_session_token);

   //
//   HTTP_CODE httpCode = client.get(url, &res);   
   HTTP_CODE httpCode = client.get(url, res);   

   // Get logger_id
   DynamicJsonBuffer jsonBuffer(512);
   JsonArray& response = jsonBuffer.parseArray(res); 
   if(response.success()) {      
      for( uint8_t idx = 0; idx < response.size(); idx++ ) {
         app_config->api_logger_id = (uint32_t) response[idx]["id"];
      }
   }

   return httpCode;
}

/*******************************************************************/
HTTP_CODE api_v1_new_logger(APP_CONFIG_STRUCT *app_config)
/* 
short:         
inputs:        
outputs:       
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   char url[128];
   //res = "";

   // Construct url
   sprintf(url, "/api/v1/clients/%d/loggers?access_token=%s", 
      app_config->api_client_id, 
      app_config->api_session_token);

   char body[128];
   sprintf(body, "{\"signature\":\"%s\"}", app_config->logger_signature);

   //
//   HTTP_CODE httpCode = client.post(url, body, &res);
   HTTP_CODE httpCode = client.post(url, body, res);

   DynamicJsonBuffer jsonBuffer(512);
   JsonObject& response = jsonBuffer.parseObject(res);
   if( response.success() ) {
      app_config->api_logger_id = (uint32_t) response["id"];
   } 

   return httpCode;
}

/*******************************************************************/
HTTP_CODE api_v1_login(APP_CONFIG_STRUCT *config)
/* 
short:         
inputs:        
outputs:       
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
//   res = "";
   res[0] = '\0';
   
   char body[128];
   sprintf(body, "{\"username\":\"%s\",\"password\":\"%s\"}", config->username, config->password);
       
//   HTTP_CODE httpCode = client.post("/api/v1/clients/login", body, &res);
   HTTP_CODE httpCode = client.post("/api/v1/clients/login", body, res);

   DynamicJsonBuffer jsonBuffer(512);
   JsonObject& response = jsonBuffer.parseObject(res);
   if( response.success() ) {
      strcpy(config->api_session_token, (const char *)(response["id"])); 
      config->api_client_id = (uint32_t) response["userId"];
   }

   return httpCode;
}

/*******************************************************************/
HTTP_CODE api_v1_postPayload(APP_CONFIG_STRUCT *config, MEASUREMENT_STRUCT *payload)
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   char url[128];
   char body[1024];

   // Construct url
   sprintf(url, "/api/v1/loggers/%d/measurements?access_token=%s", 
      config->api_logger_id, 
      config->api_session_token);

   // Construct payload
   jsonifyPayload(payload, body, 1024 );

   // Post
   HTTP_CODE httpCode = client.post(url, body, NULL);
   
   return httpCode;
}


/*******************************************************************/
void jsonifyPayload(MEASUREMENT_STRUCT *payload, char *body, int lenght )
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{   
   StaticJsonBuffer<1024> jsonBuffer;

   JsonObject& root = jsonBuffer.createObject();
   JsonObject& datagram = root.createNestedObject("datagram");
   datagram["p1"] = String(payload->p1_telegram);
   JsonObject& s0 = datagram.createNestedObject("s0");
   s0["value"] = payload->s0_value;
   s0["unit"] = "W";
   JsonObject& s1 = datagram.createNestedObject("s1");
   s1["value"] = payload->s1_value;
   s1["unit"] = "W";

   root.printTo(body, 1024);
}

/******************************************************************/
/*
 * P1 (Smart meter) section
 */
/******************************************************************/

/*******************************************************************/
void p1_store(char ch)
/* 
short:         
inputs:        
outputs:       
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   if( (p1 - p1_buf) < P1_MAX_DATAGRAM_SIZE ) {
      *p1 = ch;
      p1++; 
   } else {
      DEBUG_PRINTF("%s:P1 buffer overflow\n\r", __FUNCTION__); 
   }
}

/*******************************************************************/
void p1_reset()
/* 
short:   
inputs:        
outputs:       
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   p1 = p1_buf;
   *p1='\0';
}

/*******************************************************************/
bool capture_p1() 
/* 
short:§        Try to capture single P1 telegram         
inputs:        
outputs:       
notes:         blocking on captureLine(..) function
Version :      DMK, Initial code
*******************************************************************/
{
   bool retval = false;

   if( Serial.available() ) { 
      while( Serial.available() ) { 
         char ch = Serial.read();
         switch(p1_msg_state) {
            
            //
            case P1_MSG_S0:
               if( ch == '/' ) {
                  p1_msg_state = P1_MSG_S1;
                  p1_reset();
                  p1_store(ch);
               }
            break;             

            //
            case P1_MSG_S1:
               p1_store(ch);
               if( ch == '!' ) {
                  p1_msg_state = P1_MSG_S2;
               }
            break;

            //
            case P1_MSG_S2:
               p1_store(ch);
               if( ch == '\n' ) {
                  p1_store('\0');  // Add 0 terminator
                  p1_msg_state = P1_MSG_S0;
                  retval = true;
               }              
            break;
            
            //
            default:
               DEBUG_PRINTF("%s:Oeps, something bad happend\n\r", __FUNCTION__); 
               retval = false;
            break; 
         }
      }
   }
   return retval;
}


/******************************************************************/
/*
 * BLINK 0 and 1 ISR
 */
/******************************************************************/
 
/******************************************************************/
void blink_0_isr()
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   pulse.in_0_10_sec++;
}

/******************************************************************/
void blink_1_isr()
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{   
   pulse.in_1_10_sec++;
}

/******************************************************************/
void tickerHandler()
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{   
   DEBUG_PRINTF("%s\n\r", __FUNCTION__ );

   payload.s0_value = pulse.in_0_10_sec;
   payload.s1_value = pulse.in_1_10_sec;

   pulse.in_0_10_sec = 0;
   pulse.in_1_10_sec = 0;
}


/******************************************************************/
/*
 * Smart LED section
 */
/******************************************************************/
 
/******************************************************************/
void smartLedShowColor(RgbColor color)
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   #ifndef DEBUG 
   // Set new color
   neoPixel.SetPixelColor(0, color);
   neoPixel.Show(); 
   #endif 
}

/******************************************************************/
void smartLedFlash(uint8_t level)
/* 
short:      Flash current color         
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
   #ifndef DEBUG 
   RgbColor old = neoPixel.GetPixelColor(0);
   RgbColor flash = old;
   if(0!=old.R) flash.R = level;
   if(0!=old.G) flash.G = level;
   if(0!=old.B) flash.B = level;
   neoPixel.SetPixelColor(0, flash);
   neoPixel.Show();
   delay(50);
   neoPixel.SetPixelColor(0, old);
   neoPixel.Show();
   #endif
}

/******************************************************************/
void smartLedInit(RgbColor color)
/* 
short:      Init         
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
   #ifndef DEBUG 
   neoPixel.Begin();
   smartLedShowColor(color);
   #endif
}


/******************************************************************/
/*
 * FSM section
 */
/******************************************************************/
 
/******************************************************************/
void raiseEvent(ENUM_EVENT newEvent)
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   event = newEvent;
}


/******************************************************************/
void handleEvent()
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   ENUM_EVENT prev = event;
      
   // Call State function
   if( fsm[state][event].f != NULL) {
      fsm[state][event].f() ;
   } 
   
   // Set new state
   state = fsm[state][prev].nextState;
}

/******************************************************************/
void s0(void)
/* 
short:      Login         
inputs:        
outputs:    api_session_token and api_client_id
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
   // Clean out session_token and P1 buffer
   app_config.api_client_id = 0;
   strcpy(app_config.api_session_token, (const char *)""); 

   // P1 message buffer
   p1_reset();
   
   uint16_t http_code = api_v1_login( &app_config );
   DEBUG_PRINTF(">%s: http: %d, token: %s, user_id: %d\n\r", __FUNCTION__, http_code, app_config.api_session_token, app_config.api_client_id);
   
   switch( http_code) {
      case 200: 
         raiseEvent(EV_TRUE);
         break;
      case 401:
         api_err = ERR_INVALID_CREDENTIALS;
         raiseEvent(EV_ERR);
         break;
      default:
         api_err = ERR_NETWORK;
         raiseEvent(EV_ERR);
   }
   DEBUG_PRINTF("<%s\n\r", __FUNCTION__);
}

/******************************************************************/
void s1(void)
/* 
short:      Request api_logger_id         
inputs:        
outputs:    api_logger_id.
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
   app_config.api_logger_id = 0;
   uint16_t http_code = api_v1_loggersForClientByName( &app_config);
   DEBUG_PRINTF(">%s: http: %d, logger_id: %d\n\r", __FUNCTION__, http_code, app_config.api_logger_id);
  
      switch( http_code) {
      case 200: 
         if( 0 == app_config.api_logger_id ) {
            smartLedShowColor(blue);
            raiseEvent(EV_ILOG);
         } else  {
            smartLedShowColor(green);
            raiseEvent(EV_TRUE);
         }
         break;
      case 401:
         api_err = ERR_INVALID_CREDENTIALS;
         raiseEvent(EV_ERR);
         break;
      default:
         api_err = ERR_NETWORK;
         raiseEvent(EV_ERR);
   }
   DEBUG_PRINTF("<%s\n\r", __FUNCTION__);
}

/******************************************************************/
void s2(void)
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{  
   uint16_t http_code = api_v1_new_logger(&app_config);
   DEBUG_PRINTF(">%s: http: %d, logger_id: %d\n\r", __FUNCTION__, http_code, app_config.api_logger_id);
   switch( http_code) {
      case 200: 
         raiseEvent(EV_TRUE);
         smartLedShowColor(green);
         break;
      case 401:
         api_err = ERR_INVALID_CREDENTIALS;
         raiseEvent(EV_ERR);
         break;
      default:
         api_err = ERR_NETWORK;
         raiseEvent(EV_ERR);
   }
   DEBUG_PRINTF("<%s\n\r", __FUNCTION__);
}

/******************************************************************/
void s3(void)
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  //DEBUG_PRINTF(">%s: idle ...\n\r", __FUNCTION__);
  
  if( true == capture_p1() ) {
    cur = millis();
    uint32_t elapsed = cur - prev;
    if( elapsed >= DATAGRAM_UPDATE_RATE_MS ) {
      strcpy(payload.p1_telegram, p1_buf);
      prev = cur; 
      raiseEvent(EV_TELE);
    }
    else {
      raiseEvent(EV_TRUE);
    }
  } else {
    raiseEvent(EV_TRUE);
  }
  
  //DEBUG_PRINTF("<%s\n\r", __FUNCTION__);
}

/******************************************************************/
void s4(void)
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   static uint8_t reconnect_counter= 0;
   
   uint16_t http_code = api_v1_postPayload(&app_config, &payload);     
   DEBUG_PRINTF(">%s: http: %d\n\r", __FUNCTION__, http_code);
   switch( http_code) {
      case 0:
         //
         // Sometimes when SSL is enabled http_code equals 0. This seems
         // to be a bug. Workaround: reconnect after n times http_code = 0
         // 
         DEBUG_PRINTF(">%s: reconnect_counter: %d\n\r", __FUNCTION__, reconnect_counter);
         if( reconnect_counter++ >= 5 ) {
          reconnect_counter = 0;
          api_err = ERR_NETWORK;
          raiseEvent(EV_ERR);
         } else {
          raiseEvent(EV_TRUE);
          smartLedFlash(colorFlashLevel);
          smartLedFlash(colorFlashLevel);
         }
         break;
      case 200: 
         raiseEvent(EV_TRUE);
         smartLedFlash(colorFlashLevel);
         break;
      case 401:
         api_err = ERR_INVALID_CREDENTIALS;
         raiseEvent(EV_ERR);
         break;
      default:
         api_err = ERR_NETWORK;
         raiseEvent(EV_ERR);
   }
   
   DEBUG_PRINTF("<%s\n\r", __FUNCTION__);
}

/******************************************************************/
void err(void)
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   DEBUG_PRINTF(">%s:\n\r", __FUNCTION__);
   
   uint8_t nr_flashes = 0;
   switch(api_err) {
      case ERR_NETWORK:
         nr_flashes = 2;
         break;
      case ERR_INVALID_CREDENTIALS:
         nr_flashes = 3;
         break;         
      default:
         nr_flashes = 4;
         break;
   }

   // Flash error code
   smartLedShowColor(red);
   delay(1000);
   for( uint8_t idx = 0; idx < 2; idx++ ) {
      
      for( uint8_t idy = 0; idy < nr_flashes; idy++ ) {
         smartLedFlash(colorFlashLevel);
         delay(400);
      }
      delay(1000);
   }
   delay(1000);
   
   raiseEvent(EV_TRUE);

   DEBUG_PRINTF("<%s\n\r", __FUNCTION__);
}








