# !/usr/bin/env python

import time
import StateMachine
import EmonRestClient
from enum import Enum
import serial
import io
import threading
import queue
from neopixel import *
import logging
import os
import sys

import json
import signal

WS2812B_PIN = 18
WS2812B_NR_LEDS = 1
WS2812B_BRIGHTNESS = 55
WS2812B_IDLE_BRIGHTNESS = 20

# Message queue
msg_queue = queue.Queue(10)

class Message(Enum):
    MSG_OK = 0,
    MSG_OK_TELEGRAM_POST = 1,
    MSG_ERR_NETWORK = 2,
    MSG_ERR_INVALID_CREDENTIALS = 3,
    MSG_ERR_UNKNOWN_LOGGER = 4,
    MSG_ERR_GENERAL = 10

# Queue
p1_datagram_queue = queue.Queue(1)

# Neopixel
pixel = Adafruit_NeoPixel(num=WS2812B_NR_LEDS,
                          pin=WS2812B_PIN,
                          brightness=WS2812B_BRIGHTNESS,
                          invert=False,
                          strip_type=0x00081000)

#
format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=format)


# Config file - Server Credentials
ROOT_DIR = os.path.dirname(os.path.realpath(__file__))
CONFIG_FILE = os.path.abspath(os.path.join(ROOT_DIR, './config/emonconfig.json'))
try:
    with open(CONFIG_FILE) as config:
        app_config = json.load(config)
    config.close()
except Exception as ex:
    logging.error('Error reading config file')


#
# Capture P1 smartmeter datagrams
#
class CaptureThread(threading.Thread):

    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, verbose=None, stopEvent = None):
        super(CaptureThread, self).__init__()
        self.target = target
        self.name = name

        #
        self.stopper = stopEvent

        #
        self.ser = serial.Serial(
            port = app_config['serial']['port'],
            baudrate=app_config['serial']['baudrate'],
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=30
        )

    def run(self):
        while not self.stopper.is_set():
            try:
                #
                line = self.ser.readline().decode('ascii')

                if line[0] == '/':
                    self.output = io.StringIO()
                    self.output.write(line)
                else:
                    if line[0] == '!':
                        self.output.write(line)
                        datagram = self.output.getvalue()
                        self.output.close()
                        p1_datagram_queue.put(datagram, block=False)
                    else:
                        self.output.write(line)
            except queue.Full:
                pass
            except Exception as ex:
                logging.error('Thread {} exception: {}'.format(self.name, ex))

            time.sleep(10 / 1000.0)

        logging.debug('Thread {} ended'.format(self.name))

#
#
#
class ConsumerThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, verbose=None, stopEvent = None):
        super(ConsumerThread, self).__init__()

        self.target = target
        self.name = name

        #
        self.stopper = stopEvent

        self.begin = time.time()

        self.stateMachine = StateMachine.StateMachine()
        self.stateMachine.set_start('S_START')
        self.stateMachine.add_state('S_START', self.S_START)
        self.stateMachine.add_state('S1', self.S1)
        self.stateMachine.add_state('S2', self.S2)
        self.stateMachine.add_state('S3', self.S3)
        self.stateMachine.add_state('S_ERR', self.S_ERR)
        self.stateMachine.add_state('S_END', None, end_state = 1 )

        # Credentials
        self.credentials = app_config['credentials']

        # Logger
        self.logger = app_config['logger']

        # Init rest client
        self.client = EmonRestClient.EmonRestClient(
            url = app_config['server']['url'],
            port = app_config['server']['port'],
            api = app_config['server']['api']
        )

    def run(self):
        while not self.stopper.is_set():
            # Calculate elapsed timedif from previous
            elapsed_time = time.time() - self.begin

            # Construct json object to be send to service
            # only is elapsed_time > x seconds
            if elapsed_time >= 25.0:

                # Get Datagram if available, do not block the thread
                datagram = ''
                try:
                    datagram = p1_datagram_queue.get(block=False)
                except queue.Empty:
                    pass

                # Construct S0 and S1 values
                s0_value = 0
                s1_value = 0

                self.payload = {'datagram': {'p1': datagram,
                    's0': {'value': s0_value, 'unit': 'W'},
                    's1': {'value': s1_value, 'unit': 'W'}
                }}

                # Restart timer
                self.begin = time.time()

                # Run statemachine to upload datagram
                self.stateMachine.run()

            # Thread delay
            time.sleep(10 / 1000.0)

        logging.debug('Thread {} ended'.format(self.name))

    #
    # Login
    #
    def S_START(self):

        if self.client.validSession():
            newState = 'S3'
        else:
            username = self.credentials['username']
            password = self.credentials['password']
            http_code = self.client.login(username=username, password=password)

            if http_code == 200:
                newState = 'S1'
                msg_queue.put(Message.MSG_OK)

            elif http_code == 0:
                newState = 'S_ERR'
                msg_queue.put(Message.MSG_ERR_NETWORK)

            elif http_code == 401:
                newState = 'S_ERR'
                msg_queue.put(Message.MSG_ERR_INVALID_CREDENTIALS)

            else:
                newState = 'S_ERR'
                msg_queue.put(Message.MSG_ERR_GENERAL)

        logging.debug('{}: STATE_START'.format(self.name))
        return newState

    #
    # Get logger_id for signature
    #
    def S1(self):
        http_code = self.client.loggersForClientBySignature( signature = self.logger['signature'] )

        if http_code == 0:
            newState = 'S_ERR'
            msg_queue.put(Message.MSG_ERR_NETWORK)

        elif http_code == 200:
            newState = 'S3'
            msg_queue.put(Message.MSG_OK)

        elif http_code == 201:
            newState = 'S2'
            msg_queue.put(Message.MSG_ERR_UNKNOWN_LOGGER)

        else:
            newState = 'S_ERR'
            msg_queue.put(Message.MSG_ERR_GENERAL)

        logging.debug('{}: S1'.format(self.name))
        return newState

    # Register new logger with signature
    def S2(self):
        http_code = self.client.registerLoggerWithSignature(signature = self.logger['signature'])

        if http_code == 0:
            newState = 'S_ERR'
            msg_queue.put(Message.MSG_ERR_NETWORK)

        elif http_code == 200:
            newState = 'S1'
            msg_queue.put(Message.MSG_OK)

        else:
            newState = 'S_ERR'
            msg_queue.put(Message.MSG_ERR_GENERAL)

        logging.debug('{}: S2'.format(self.name))
        return newState

    # Post datagram
    def S3(self):

        http_code = self.client.postPayload(self.payload)

        if http_code == 0:
            newState = 'S_ERR'
            msg_queue.put(Message.MSG_ERR_NETWORK)

        elif http_code == 200:
            newState = 'S_END'
            msg_queue.put(Message.MSG_OK_TELEGRAM_POST)

        else:
            newState = 'S_ERR'
            msg_queue.put(Message.MSG_ERR_GENERAL)

        logging.debug('{}: S3. Payload: {}'.format(self.name, self.payload))
        return newState

    # Error state
    def S_ERR(self):
        newState = 'S_END'
        self.client.invalidateSession()

        logging.debug('{}: S_ERR'.format(self.name))
        return newState

#
#
#
class WS2812BThread(threading.Thread):

    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, verbose=None, stopEvent=None):
        super(WS2812BThread, self).__init__()
        self.target = target
        self.name = name

        #
        self.stopper = stopEvent

        #
        pixel.setPixelColor(0, Color(0, WS2812B_IDLE_BRIGHTNESS, 0))
        pixel.show()

    def run(self):
        while not self.stopper.is_set():

            if not msg_queue.empty():
                msg = msg_queue.get()

                if msg == Message.MSG_ERR_GENERAL:
                    self.neopixelFlash( color = Color(red=WS2812B_IDLE_BRIGHTNESS, green=0, blue=0), flashes = 1 )

                elif msg == Message.MSG_ERR_INVALID_CREDENTIALS:
                    self.neopixelFlash( color = Color(red=WS2812B_IDLE_BRIGHTNESS, green=0, blue=0), flashes = 2 )

                elif msg == Message.MSG_ERR_NETWORK:
                    self.neopixelFlash( color = Color(red=WS2812B_IDLE_BRIGHTNESS, green=0, blue=0), flashes = 3 )

                elif msg == Message.MSG_ERR_UNKNOWN_LOGGER:
                    self.neopixelFlash( color = Color(red=WS2812B_IDLE_BRIGHTNESS, green=0, blue=0), flashes = 4 )

                elif msg == Message.MSG_OK_TELEGRAM_POST:
                    self.neopixelFlash( color = Color(red=0, green=WS2812B_IDLE_BRIGHTNESS, blue=0), flashes = 1 )

                elif msg == Message.MSG_OK:
                    pixel.setPixelColor(0, Color(red=0, green=WS2812B_IDLE_BRIGHTNESS, blue=0))
                    pixel.show()

                logging.debug('{}: {}'.format(self.name, msg))

            else:
                pass
            time.sleep(1)

        logging.debug('Thread {} ended'.format(self.name))

    #
    def neopixelFlash(self, color, flashes):

        r = (color >> 16) & 0xFF
        g = (color >> 8) & 0xFF
        b = (color >> 0) & 0xFF

        if r != 0:
            r = 255

        if g != 0:
            g = 255

        if b != 0:
            b = 255

        hl = Color(red = r, green = g, blue = b)

        for x in range(0, flashes):
            pixel.setPixelColor(0, color)
            pixel.show()
            time.sleep(0.3)
            pixel.setPixelColor(0, hl)
            pixel.show()
            time.sleep(0.1)

        pixel.setPixelColor(0, color)
        pixel.show()


# Handle signals
def signal_handler(signal, frame):
    # p1.isRunning = False
    # p2.isRunning = False
    # p3.isRunning = False
    for t in threads:
        t.stopper.set()

    # pass

if __name__ == '__main__':

    # Set signal handler
    signal.signal(signal.SIGINT, signal_handler)

    # Thread stopper
    stopper = threading.Event()

    pixel = Adafruit_NeoPixel(num=WS2812B_NR_LEDS,
                              pin=WS2812B_PIN,
                              brightness=WS2812B_BRIGHTNESS,
                              invert=False,
                              strip_type=0x00081000)
    pixel.begin()
    pixel.setPixelColor(0, Color(red=0, green=0, blue=0))
    pixel.show()

    # Do the color loop
    colors = [
        Color(red=0xFF, green=0x00, blue=0x00),
        Color(red=0xFF, green=0xFF, blue=0x00),
        Color(red=0x00, green=0xFF, blue=0x00),
        Color(red=0x00, green=0xFF, blue=0xFF),
        Color(red=0x00, green=0x00, blue=0xFF),
        Color(red=0x00, green=0x00, blue=0x00)
    ]
    for x in range(0, 4):
        for color in colors:
            pixel.setPixelColor(0, color)
            pixel.show()
            time.sleep(0.1)

    # Declare and start all threads
    threads = [
        CaptureThread(name='P1Caputure', stopEvent=stopper),
        ConsumerThread(name='Consumer', stopEvent=stopper),
        WS2812BThread(name='ws2812b', stopEvent=stopper),
    ]

    # p1 = CaptureThread(name='P1Caputure')
    # p2 = ConsumerThread(name='Consumer')
    # p3 = WS2812BThread(name='ws2812b')

    for t in threads:
        t.start()
    # p1.start()
    # p2.start()
    # p3.start()

    #
    for t in threads:
        t.join()

    # p1.join()
    # p2.join()
    # p3.join()

    #
    logging.debug("Bye")

