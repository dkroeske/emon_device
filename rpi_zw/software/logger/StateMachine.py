
class InitializationError(Exception):
    def __init__(self, code):
        self.code = code
    def __str__(self):
        return repr(self.code)

# Simple statemachine: see http://www.python-course.eu/finite_state_machine.php
class StateMachine:

    def __init__(self):
        self.handlers = {}
        self.startState = None
        self.endStates = []

    def add_state(self, name, handler, end_state = 0):
        self.handlers[name] = handler
        if end_state:
            self.endStates.append(name)

    def set_start(self, name):
        self.startState = name

    def run(self):

        try:
            handler = self.handlers[self.startState]
        except:
            raise InitializationError("must call .set_start() before .run()")
        if not self.endStates:
            raise  InitializationError("at least one state must be an end_state")

        while True:
            newState = handler()

            if newState in self.endStates:
                break
            else:
                handler = self.handlers[newState]



