import time;
import requests
import json
import urllib

class EmonRestClient:
    def __init__(self, url, port, api):

        self.base_url = url + ':' + port + '/api/' + api + '/'
        self.headers = {'content-type': 'application/json'}
        self.session = {}

    def validSession(self):
        if not self.session:
            return False
        else:
            return True

    def invalidateSession(self):
        self.session.clear()


    # Login - get Token
    def login(self, username, password):
        retval = None

        try:
            url = self.base_url + 'clients/login'
            payload = {'username': username, 'password': password}
            req = requests.post(url, data=json.dumps(payload), headers = self.headers)
        #    req.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print(err)
        except requests.exceptions.RequestException as err:
            print(err)
        else:
            self.session.clear()
            retval = req.status_code
            try:
                self.session['token'] = req.json()['id']
                self.session['user_id'] = req.json()['userId']
            except:
                retval = None
        return retval


    #
    # Register logger with signature for user with _id
    #
    def registerLoggerWithSignature(self, signature):
        retval = None
        try:
            url = self.base_url + 'clients/{0!s}/loggers?access_token={1}'.format(
                self.session['user_id'],
                self.session['token']
            )
            payload = {'signature': signature }
            req = requests.post(url, data=json.dumps(payload), headers=self.headers)
        except requests.exceptions.HTTPError as err:
            print(err)
        except requests.exceptions.RequestException as err:
            print(err)
        else:
            retval = req.status_code
        return retval


    # Get loggers for clients for given signature
    def loggersForClientBySignature(self, signature):
        retval = None
        try:
            filter = {'where':{'signature': signature}}
            filter = json.dumps(filter)
            url = self.base_url + 'clients/{0!s}/loggers?filter={1}&access_token={2}'.format(
                self.session['user_id'],
                urllib.parse.quote(filter),
                self.session['token']
            )
            req = requests.get(url, headers = self.headers)
        except requests.exceptions.HTTPError as err:
            print(err)
        except requests.exceptions.RequestException as err:
            print(err)
        else:
            retval = req.status_code
            try:
                self.session['logger_id'] = req.json()[0]['id']
            except:
                retval = 201

        return retval

    # Post Payload
    def postPayload(self, payload):
        retval = None
        try:
            url = self.base_url + 'loggers/{0!s}/measurements?access_token={1}'.format(
                self.session['logger_id'],
                self.session['token']
            )
            req = requests.post(url, data=json.dumps(payload), headers = self.headers)
        except requests.exceptions.HTTPError as err:
            print(err)
        except requests.exceptions.RequestException as err:
            print(err)
        else:
            retval = req.status_code
        return retval
